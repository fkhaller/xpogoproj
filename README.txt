How to setup project:

- Go to cryengine.com and install the engine and launcher

- Pull this repo onto your computer at any desired location

- Right click on the cryengine project file and select generate solution

- Navigate to Solutions/RollingBallTemplate.win_x64

- Open RollingBallTemplate visual studio solution file in Visual Studio 2013 or later

- Build the solution

- Open the Cryengine launcher and select import project. Project should be imported as 
Rolling Ball Game Template
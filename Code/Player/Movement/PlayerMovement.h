#pragma once

#include "Entities/Helpers/ISimpleExtension.h"

class CPlayer;

////////////////////////////////////////////////////////
// Player extension to manage movement
////////////////////////////////////////////////////////
class CPlayerMovement : public CGameObjectExtensionHelper<CPlayerMovement, ISimpleExtension>
{
public:
	CPlayerMovement();
	virtual ~CPlayerMovement() {}

	//ISimpleExtension
	virtual void PostInit(IGameObject* pGameObject) override;

	virtual void Update(SEntityUpdateContext& ctx, int updateSlot) override;


	//~ISimpleExtension

	void BounceCheck();
	void UpdateRotation();
	void UpdatePosition();
	void CrashCheck();
	void Crash();

	f32 GetZRotation(){ return m_fZRot; };

protected:
	CPlayer *m_pPlayer;
	IEntity *m_pBody;

	Vec3 m_vUp;
	Vec3 m_vDown;

	f32 m_fGroundHeight;

	Vec3 m_vVel;
	f32 m_fStartUpVel;
	
	//stick rotation
	f32 m_fXRot;
	f32 m_fYRot;
	f32 m_fZRot;

	//body rotation
	f32 m_fBXRot;
	f32 m_fBYRot;
	f32 m_fBZRot;

	int speed_cnt;

	bool m_bMadeContactThisBounce;
	bool m_bJumpedThisBounce;

	bool m_bStartBounce;
	bool m_bUpBounce;

	bool m_bSpinForward;
	bool m_bSpinBack;
	bool m_bSpin;

	bool m_bRecoil;
	Vec3 m_vRecoil;
	f32 m_fRecoilStrength;

	//score keeping
	int total_score;
	int current_score;
	f32 rotation_score;
	char * trick_name;
	f32 max_detached_angle;
	int time_detached;
};

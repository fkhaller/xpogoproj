#include "StdAfx.h"
#include "PlayerMovement.h"

#include "Player/Player.h"
#include "Player/Input/PlayerInput.h"
#include "Player/View/PlayerView.h"

#define SPEED 0 // 0 = normal, 1 = half speed, >= 2 gets progressivly slower
#define GRAVITY_CONST .02
#define BOUNCE_STRENGTH .08 
#define ROTATION_STRENGTH .02

CPlayerMovement::CPlayerMovement()
	: m_bJumpedThisBounce(false),
	  m_bMadeContactThisBounce(false),
	  m_bStartBounce(false),
	  m_bUpBounce(false),
	  m_bSpinForward(false),
	  m_bSpinBack(false),
	  m_bSpin(false),
	  m_fXRot(0),
	  m_fYRot(0),
	  m_fZRot(0),
	  m_fBXRot(0),
	  m_fBYRot(0),
	  m_fBZRot(0),
	  m_fStartUpVel(0),
	  m_vVel(0,0,0),
	  m_fGroundHeight(32),
	  speed_cnt(SPEED),
	  m_vRecoil(ZERO),
	  m_bRecoil(false),
	  total_score(0),
	  current_score(0),
	  rotation_score(0)
{
}

void CPlayerMovement::PostInit(IGameObject *pGameObject)
{
	m_pPlayer = static_cast<CPlayer *>(pGameObject->QueryExtension("Player"));

	// Make sure that this extension is updated regularly via the Update function below
	pGameObject->EnableUpdateSlot(this, 0);
}

void CPlayerMovement::Update(SEntityUpdateContext &ctx, int updateSlot)
{
	//slow motion for testing
	if (speed_cnt >= 0){
		m_vUp = GetEntity()->GetRotation().GetColumn2().normalized();
		m_vDown = Vec3(-m_vUp.x, -m_vUp.y, -m_vUp.z);

		//this will represent the bottom of pogo stick
		//IPersistantDebug* debug = gEnv->pGame->GetIGameFramework()->GetIPersistantDebug();
		//debug->AddDirection(GetEntity()->GetPos(), m_vDown.GetLength(), m_vDown, ColorF(255, 255, 255), .05f);

		//apply gravity
		m_vVel += Vec3(0, 0, -GRAVITY_CONST);

		if (m_vVel.z < 0 && m_bUpBounce){
			m_bUpBounce = false;
		}
		
		if (!m_bUpBounce){
			BounceCheck();
		}

		UpdateRotation();

		UpdatePosition();

		CrashCheck();

		if (speed_cnt > 0){
			speed_cnt--; 
		}
	}
	else{
		speed_cnt = SPEED;
	}

	gEnv->pGame->GetIGameFramework()->GetIPersistantDebug()->Add2DText(ToString(total_score), 8, ColorF(255, 255, 255), gEnv->pTimer->GetFrameTime());

}

void CPlayerMovement::BounceCheck(){
	ray_hit hit;

	int h = gEnv->pPhysicalWorld->RayWorldIntersection(GetEntity()->GetPos(),2 * m_vDown, ent_all, rwi_any_hit, &hit, 1);
	
	if (h){

		if (!m_bMadeContactThisBounce){
			m_bMadeContactThisBounce = true;
			m_bJumpedThisBounce = false;
		}

		if (hit.dist > 1){

			if (!m_bRecoil){
				m_fRecoilStrength = m_vVel.GetLength();
				m_bRecoil = true;
			}

			m_bSpinForward = false;
			m_bSpinBack = false;
			m_bSpin = false;

			m_vVel = Vec3(0, 0, -GRAVITY_CONST * 4);
			m_vRecoil -= Vec3(0, 0, GRAVITY_CONST / 3) * m_fRecoilStrength;
			
			if (Vec3(0, 0, 1).Dot(hit.n) < .95){

				if (hit.n.Dot(m_vUp) < .85){
					m_vVel = Vec3(hit.n.x / 2, hit.n.y / 2, -hit.n.z * GRAVITY_CONST * 7);
					m_vRecoil = Vec3(0, 0, 0);
					current_score = 10;
					trick_name = "Slide";
				}
				
				
			}

			if (m_fBXRot > gf_PI / 10 || m_fBYRot > gf_PI / 10 || m_fBYRot < -gf_PI / 10){
				Crash();
			}

		}
		else{
			//start the bounce
			m_bStartBounce = true;
			m_bRecoil = false;
			m_pPlayer->GetInput()->RemoveInputFlags(CPlayerInput::eInputFlag_ToggleBodyLock | CPlayerInput::eInputFlag_Rotate);

			f32 diff = GetEntity()->GetPos().z - m_fGroundHeight;
			int dir;

			if (diff > 1){
				if (diff > 0){
					dir = 1;
				}
				else{
					dir = -1;
				}

				m_fStartUpVel -= 3 * dir * sqrt(abs(diff)) * BOUNCE_STRENGTH;

			}
			
			m_fGroundHeight = GetEntity()->GetPos().z;

			//calculate detached points
			if (max_detached_angle > 0){
				current_score += max_detached_angle * 100;
				trick_name = "Detach";
			}
			
			//flip score based on upright landing
			if (abs(rotation_score) > 3 * gf_PI / 2){
				if (current_score > 50){
					current_score *= 2;
					trick_name = "Flip + Detach";
				}
				else{
					trick_name = "Flip";
				}
				current_score += 1000 * hit.n.Dot(m_vUp);

			}

			total_score += current_score;
			if (current_score > 0){
				gEnv->pGame->GetIGameFramework()->GetIPersistantDebug()->Add2DText("+" + ToString(current_score) + " " + trick_name, 8, ColorF(255, 255, 255), 1.5);
			}
			current_score = 0;
			rotation_score = 0; 
			max_detached_angle = 0;
			m_fBYRot = 0;
			m_fBXRot = 0;
		}
	}

	
}

void CPlayerMovement::UpdateRotation(){

	auto inputFlags = m_pPlayer->GetInput()->GetInputFlags();


	if (inputFlags & CPlayerInput::eInputFlag_SpinForward){
		if (m_bSpinForward == false){
			m_bSpinForward = true;
		}
	}
	if (inputFlags & CPlayerInput::eInputFlag_SpinBack){
		if (m_bSpinBack == false){
			m_bSpinBack = true;
		}
	}
	if (inputFlags & CPlayerInput::eInputFlag_Spin){
		if (m_bSpin == false){
			m_bSpin = true;
		}
	}

	if (!m_bSpinBack && !m_bSpinForward){
		//check for bounce attempt
		if (inputFlags & CPlayerInput::eInputFlag_Jump)
		{
			//jump initiation
			if (!m_bJumpedThisBounce){
				m_bJumpedThisBounce = true;
				if (m_fStartUpVel == 0){
					m_bStartBounce = true;
				}
			}
		}
		if (inputFlags & CPlayerInput::eInputFlag_MoveForward)
		{
			m_fXRot -= ROTATION_STRENGTH;
			rotation_score += ROTATION_STRENGTH;
		}
		if (inputFlags & CPlayerInput::eInputFlag_MoveBack)
		{
			m_fXRot += ROTATION_STRENGTH;
			rotation_score -= ROTATION_STRENGTH;
		}
		if (inputFlags & CPlayerInput::eInputFlag_MoveLeft)
		{
			m_fYRot -= ROTATION_STRENGTH;

		}
		if (inputFlags & CPlayerInput::eInputFlag_MoveRight)
		{
			m_fYRot += ROTATION_STRENGTH;

		}
	}
	else{

		m_fXRot -= 4 * ROTATION_STRENGTH;
		m_fXRot += 4 * ROTATION_STRENGTH;

		rotation_score += 4 * ROTATION_STRENGTH;
		rotation_score -= 4 * ROTATION_STRENGTH;

	}

	if (!m_bSpin){
		if (inputFlags & CPlayerInput::eInputFlag_Rotate){
			m_fZRot -= m_pPlayer->GetInput()->GetSpinPower() * ROTATION_STRENGTH;
		}
	}
	else{
		m_fZRot -= 2 * m_pPlayer->GetInput()->GetSpinPower() * ROTATION_STRENGTH;
	}
	
	//smooth full rotations
	if (m_fXRot > gf_PI || m_fXRot < -gf_PI){
		m_fXRot = -m_fXRot;
	}

	Ang3 rotation;
	rotation.x = m_fXRot;
	rotation.y = m_fYRot;
	rotation.z = m_fZRot;

	//set stick rotation
	GetEntity()->SetRotation(
		 Quat(rotation));

	if (inputFlags & CPlayerInput::eInputFlag_ToggleBodyLock){
		if (inputFlags & CPlayerInput::eInputFlag_RotateBodyRight){
			m_fBYRot -= 1.5 * ROTATION_STRENGTH;
		}
		if (inputFlags & CPlayerInput::eInputFlag_RotateBodyLeft){
			m_fBYRot += 1.5 * ROTATION_STRENGTH;
		}
		if (inputFlags & CPlayerInput::eInputFlag_RotateBodyForward){
			m_fBXRot -= 1.5 * ROTATION_STRENGTH;
		}
		if (inputFlags & CPlayerInput::eInputFlag_RotateBodyBack){
			m_fBXRot += 1.5 * ROTATION_STRENGTH;
		}
	}
	else{
		if (m_fBYRot < 0){
			m_fBYRot += 1.5 * ROTATION_STRENGTH;
		}
		if (m_fBYRot > 0){
			m_fBYRot -= 1.5 * ROTATION_STRENGTH;
		}
		if (m_fBXRot > 0){
			m_fBXRot -= 1.5 * ROTATION_STRENGTH;
		}
		m_pPlayer->GetInput()->RemoveInputFlags(CPlayerInput::eInputFlag_RotateBodyForward | CPlayerInput::eInputFlag_RotateBodyBack 
			| CPlayerInput::eInputFlag_RotateBodyRight | CPlayerInput::eInputFlag_RotateBodyLeft);
	}

	//clamp the rotation
	if (m_fBYRot > gf_PI / 2){
		m_fBYRot -= ROTATION_STRENGTH;
	}
	if (m_fBYRot < -gf_PI / 2){
		m_fBYRot += ROTATION_STRENGTH;
	}

	if (m_fBXRot > gf_PI / 2){
		m_fBXRot -= ROTATION_STRENGTH;
	}
	if (m_fBXRot < 0){
		m_fBXRot += ROTATION_STRENGTH;
	}

	//set body rotation
	gEnv->pEntitySystem->GetEntity(5)->SetRotation
		(GetEntity()->GetRotation() 
		* Quat::CreateRotationY(m_fBYRot) 
		* Quat::CreateRotationX(-m_fBXRot + gf_PI)
		* Quat::CreateRotationZ(m_fBZRot));

	//keep track of max detached angle for point calculations
	f32 current_detached_angle = abs(m_fBXRot) > abs(m_fBYRot) ? abs(m_fBXRot) : abs(m_fBYRot);

	if (current_detached_angle > max_detached_angle){
		max_detached_angle = current_detached_angle;
	}

	gEnv->pEntitySystem->GetEntity(6)->SetRotation(GetEntity()->GetWorldRotation());
}

void CPlayerMovement::UpdatePosition(){

	if (m_bStartBounce){

		if (m_fStartUpVel < 0){
			m_fStartUpVel = 0;
		}

		if (m_bJumpedThisBounce){
			m_fStartUpVel += 3 * BOUNCE_STRENGTH;

			if (m_fStartUpVel > 12 * BOUNCE_STRENGTH){
				m_fStartUpVel = 12 * BOUNCE_STRENGTH;
			}
		}
		//no bounce attampt was made
		else if (m_fStartUpVel > 0){
			m_fStartUpVel -= BOUNCE_STRENGTH;
		}

		m_vVel = m_fStartUpVel * m_vUp;
		
		m_bStartBounce = false;
		m_bMadeContactThisBounce = false;
		m_bUpBounce = true;
	}
	Vec3 newPos = GetEntity()->GetPos();
	newPos += m_vVel;

	GetEntity()->SetPos(newPos);

	if (!m_bRecoil && m_vRecoil.z < 0){
		m_vRecoil += Vec3(0, 0, GRAVITY_CONST / 3) * m_fRecoilStrength;
	}

	gEnv->pEntitySystem->GetEntity(5)->SetPos(newPos - .5 * GetEntity()->GetForwardDir() + 2.3 * m_vUp + m_vRecoil);

	if (!m_bRecoil){
		gEnv->pEntitySystem->GetEntity(6)->SetPos(GetEntity()->GetWorldPos() + Vec3(0, 0, -1));
	}
}

void CPlayerMovement::CrashCheck(){

	primitives::cylinder cylinder;
	cylinder.axis = m_vUp;
	cylinder.center = GetEntity()->GetPos() + 1.5 * m_vUp;
	cylinder.r = .3f;
	cylinder.hh = 1;

	//gEnv->pGame->GetIGameFramework()->GetIPersistantDebug()->AddCylinder(cylinder.center, cylinder.axis,cylinder.r,cylinder.hh, ColorF(255, 255, 255), .05f);
	geom_contact* pContact;

	bool i = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(cylinder.type, &cylinder,Vec3(ZERO),ent_all,&pContact) > 0.0f;

	if (GetEntity()->GetPos().z < 32 || i ){
		//reset
		Crash();
	}

}

void CPlayerMovement::Crash(){
	CryLog("crash");
	m_pPlayer->SetHealth(0);
	m_fXRot = 0;
	m_fYRot = 0;
	m_fZRot = 0;
	m_fBXRot = 0;
	m_fBYRot = 0;
	m_fBZRot = 0;
	m_vVel = Vec3(0, 0, 0);
	m_fStartUpVel = 0;
	m_bSpinForward = false;
	m_bSpinBack = false;
	speed_cnt = 0;
	current_score = 0;
	total_score = 0;
	rotation_score = 0;
	max_detached_angle = 0;
	gEnv->pGame->GetIGameFramework()->GetIPersistantDebug()->Add2DText("YOU CRASHED!", 8, ColorF(255, 255, 255), 3);
}
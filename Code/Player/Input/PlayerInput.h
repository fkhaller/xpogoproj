#pragma once

#include "Entities/Helpers/ISimpleExtension.h"

class CPlayer;

////////////////////////////////////////////////////////
// Player extension to manage input
////////////////////////////////////////////////////////
class CPlayerInput 
	: public CGameObjectExtensionHelper<CPlayerInput, ISimpleExtension>
	, public IActionListener
{
	enum EInputFlagType
	{
		eInputFlagType_Hold = 0,
		eInputFlagType_Toggle,
		eInputFlagType_Trigger
	};

public:
	typedef uint16 TInputFlags;

	enum EInputFlags
		: TInputFlags
	{
		eInputFlag_MoveLeft          = 1 << 0,
		eInputFlag_MoveRight         = 1 << 1,
		eInputFlag_MoveForward       = 1 << 2,
		eInputFlag_MoveBack          = 1 << 3,
		eInputFlag_Jump              = 1 << 4,
		eInputFlag_SpinForward       = 1 << 5,
		eInputFlag_SpinBack          = 1 << 6,
		eInputFlag_RotateBodyLeft    = 1 << 7,
		eInputFlag_RotateBodyRight   = 1 << 8,
		eInputFlag_RotateBodyForward = 1 << 9,
		eInputFlag_RotateBodyBack    = 1 << 10,
		eInputFlag_ToggleBodyLock    = 1 << 11,
		eInputFlag_Rotate            = 1 << 12,
		eInputFlag_Spin              = 1 << 13
	};

public:
	virtual ~CPlayerInput() {}

	// ISimpleExtension
	virtual void PostInit(IGameObject* pGameObject) override;
	
	virtual void HandleEvent(const SGameObjectEvent &event) override;

	virtual void Update(SEntityUpdateContext &ctx, int updateSlot) override;
	// ~ISimpleExtension

	// IActionListener
	virtual void OnAction(const ActionId &action, int activationMode, float value) override;
	// ~IActionListener

	void OnPlayerRespawn();

	const TInputFlags GetInputFlags() const { return m_inputFlags; }
	void RemoveInputFlags(int i) { m_inputFlags &= ~i; }
	void ResetInputFlags(){ m_inputFlags = 0; }

	const Vec2 GetMouseDeltaRotation() const { return m_mouseDeltaRotation; }

	const Quat &GetLookOrientation() const { return m_lookOrientation; }

	const f32 &GetSpinPower() const { return m_fSpinPower; }

protected:
	void InitializeActionHandler();

	void HandleInputFlagChange(EInputFlags flags, int activationMode, EInputFlagType type = eInputFlagType_Hold);

	// Start actions below
protected:
	bool OnActionMoveLeft(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionMoveRight(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionMoveForward(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionMoveBack(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionJump(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionTrick(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionToggleBodyLock(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionRotate(EntityId entityId, const ActionId& actionId, int activationMode, float value);

	bool OnActionMouseRotateYaw(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionMouseRotatePitch(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	
protected:
	CPlayer *m_pPlayer;

	TInputFlags m_inputFlags;

	Vec2 m_mouseDeltaRotation;

	Quat m_lookOrientation;

	f32 m_fSpinPower;

	// Handler for actionmap events that maps actions to callbacks
	TActionHandler<CPlayerInput> m_actionHandler;
};

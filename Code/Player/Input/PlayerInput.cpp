#include "StdAfx.h"
#include "PlayerInput.h"

#include "Player/Player.h"

void CPlayerInput::PostInit(IGameObject *pGameObject)
{
	const int requiredEvents[] = { eGFE_BecomeLocalPlayer };
	pGameObject->UnRegisterExtForEvents(this, NULL, 0);
	pGameObject->RegisterExtForEvents(this, requiredEvents, sizeof(requiredEvents) / sizeof(int));

	m_pPlayer = static_cast<CPlayer *>(pGameObject->QueryExtension("Player"));

	// Populate the action handler callbacks so that we get action map events
	InitializeActionHandler();
}

void CPlayerInput::HandleEvent(const SGameObjectEvent &event)
{
	if (event.event == eGFE_BecomeLocalPlayer)
	{
		IActionMapManager *pActionMapManager = gEnv->pGame->GetIGameFramework()->GetIActionMapManager();

		pActionMapManager->InitActionMaps("Libs/config/defaultprofile.xml");
		pActionMapManager->Enable(true);

		pActionMapManager->EnableActionMap("player", true);

		if(IActionMap *pActionMap = pActionMapManager->GetActionMap("player"))
		{
			pActionMap->SetActionListener(GetEntityId());
		}

		GetGameObject()->CaptureActions(this);

		// Make sure that this extension is updated regularly via the Update function below
		GetGameObject()->EnableUpdateSlot(this, 0);
	}
}

void CPlayerInput::Update(SEntityUpdateContext &ctx, int updateSlot)
{
	// Start by updating look dir
	Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(m_lookOrientation));
	
	//camera should line up with player direction
	ypr.x = m_pPlayer->GetMovement()->GetZRotation();

	//lock camera in a position dynamic camera is low priority
	ypr.y = -.5;
	ypr.z = 0;

	m_lookOrientation = Quat(CCamera::CreateOrientationYPR(ypr));

	// Reset every frame
	m_mouseDeltaRotation = ZERO;
	m_fSpinPower = 0;
}

void CPlayerInput::OnPlayerRespawn()
{
	m_inputFlags = 0;
	m_mouseDeltaRotation = ZERO;
	m_lookOrientation = IDENTITY;
}

void CPlayerInput::HandleInputFlagChange(EInputFlags flags, int activationMode, EInputFlagType type)
{
	switch (type)
	{
		case eInputFlagType_Hold:
		{
			if (activationMode == eIS_Released)
			{
				m_inputFlags &= ~flags;
			}
			else
			{
				m_inputFlags |= flags;
			}
		}
		break;
		case eInputFlagType_Toggle:
		{
			if (activationMode == eIS_Pressed)
			{
				// Toggle the bit(s)
				m_inputFlags ^= flags;
			}
		}
		break;
	}
}

void CPlayerInput::InitializeActionHandler()
{
	m_actionHandler.AddHandler(ActionId("moveleft"), &CPlayerInput::OnActionMoveLeft);
	m_actionHandler.AddHandler(ActionId("moveright"), &CPlayerInput::OnActionMoveRight);
	m_actionHandler.AddHandler(ActionId("moveforward"), &CPlayerInput::OnActionMoveForward);
	m_actionHandler.AddHandler(ActionId("moveback"), &CPlayerInput::OnActionMoveBack);
	m_actionHandler.AddHandler(ActionId("jump"), &CPlayerInput::OnActionJump);
	m_actionHandler.AddHandler(ActionId("flip"), &CPlayerInput::OnActionTrick);
	m_actionHandler.AddHandler(ActionId("toggleLock"), &CPlayerInput::OnActionToggleBodyLock);
	m_actionHandler.AddHandler(ActionId("rotate"), &CPlayerInput::OnActionRotate);

	m_actionHandler.AddHandler(ActionId("mouse_rotateyaw"), &CPlayerInput::OnActionMouseRotateYaw);
	m_actionHandler.AddHandler(ActionId("mouse_rotatepitch"), &CPlayerInput::OnActionMouseRotatePitch);
}

void CPlayerInput::OnAction(const ActionId &action, int activationMode, float value)
{
	// This function is called when inputs trigger action map events
	// The handler below maps the event (see 'action') to a callback, further below in this file.
	m_actionHandler.Dispatch(this, GetEntityId(), action, activationMode, value);
}

bool CPlayerInput::OnActionMoveLeft(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	HandleInputFlagChange(eInputFlag_MoveLeft, activationMode);
	return true;
}

bool CPlayerInput::OnActionMoveRight(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	HandleInputFlagChange(eInputFlag_MoveRight, activationMode);
	return true;
}

bool CPlayerInput::OnActionMoveForward(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	HandleInputFlagChange(eInputFlag_MoveForward, activationMode);
	return true;
}

bool CPlayerInput::OnActionMoveBack(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	HandleInputFlagChange(eInputFlag_MoveBack, activationMode);
	return true;
}

bool CPlayerInput::OnActionJump(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	HandleInputFlagChange(eInputFlag_Jump, activationMode);
	return true;
}

bool CPlayerInput::OnActionTrick(EntityId entityId, const ActionId& actionId, int activationMode, float value){

	if (activationMode == eAAM_OnPress){
		if (m_inputFlags & eInputFlag_MoveForward){
			m_inputFlags |= eInputFlag_SpinForward;
		}
		else if (m_inputFlags & eInputFlag_MoveBack){
			m_inputFlags |= eInputFlag_SpinBack;
		}
		else if (m_inputFlags & eInputFlag_Rotate){
			m_inputFlags |= eInputFlag_Spin;
		}
	}
	
	if (activationMode == eAAM_OnRelease){
		m_inputFlags &= ~(eInputFlag_SpinForward | eInputFlag_SpinBack | eInputFlag_Spin);
	}

	return true;
}

bool CPlayerInput::OnActionToggleBodyLock(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	m_inputFlags ^= eInputFlag_ToggleBodyLock;
	return true;
}

bool CPlayerInput::OnActionRotate(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	if (activationMode == eAAM_OnPress)
		m_inputFlags |= eInputFlag_Rotate;
	else if (activationMode == eAAM_OnRelease)
		m_inputFlags &= ~eInputFlag_Rotate;
	return true;
}

bool CPlayerInput::OnActionMouseRotateYaw(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{

	m_fSpinPower = clamp_tpl(value, (f32)-5, (f32)5);

	if (value > 50){
		m_inputFlags |= eInputFlag_RotateBodyRight;
		m_inputFlags &= ~eInputFlag_RotateBodyLeft;
	}
	else if (value < -50){
		m_inputFlags |= eInputFlag_RotateBodyLeft;
		m_inputFlags &= ~eInputFlag_RotateBodyRight;
	}
	return true;
}

bool CPlayerInput::OnActionMouseRotatePitch(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	if (value > 50){
		m_inputFlags |= eInputFlag_RotateBodyBack;
		m_inputFlags &= ~eInputFlag_RotateBodyForward;
	}
	else if (value < -50){
		m_inputFlags |= eInputFlag_RotateBodyForward;
		m_inputFlags &= ~eInputFlag_RotateBodyBack;
	}
	return true;
}